**DicomManager**

The DicomManager.py script receives a URL as a command-line argument and downloads the file.

*Then it rearrange the files according to the DICOM hierarchy in an appropriate directory structure (patient/study/series).*

*And performs the following tasks:*

1. Generating a list of patients, their age and sex
2. Showing CT scan average duration 
3. Showing the number of different hospitals that the data came from


