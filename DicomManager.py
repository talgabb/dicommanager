import urllib.request
import tarfile
import pydicom as dicom
import os


def average(lst):
    return sum(lst) / len(lst)


def patient_printer(patients):
    # Prints the the dictionary in cleaner way
    for name in patients:
        patient = patients[name]
        print(f"name:{name} \t age:{patient['Age']} \t Sex:{patient['Sex']}")


url = input()

# Downloading file from URL
print('downloading..')
urllib.request.urlretrieve(url, "raw_data.tgz")

# Setting variables for saved data
patient_data = {}
hospitals = set()
scan_duration = []

print('extracting files...')
with tarfile.open('raw_data.tgz', 'r') as archive:

    file_names = archive.getnames()
    members = archive.getmembers()
    # Looping through the files and extract each one
    for f in range(len(file_names)):
        raw_file = archive.extractfile(members[f])
        file = dicom.read_file(raw_file)

        # saving data for desired question
        patient_data[file.PatientName] = {'Age': file.PatientAge, 'Sex': file.PatientSex}
        hospitals.update({file.InstitutionName})
        scan_duration.append(float(file.ContentTime) - float(file.StudyTime))

        # Writing file to desired destination
        path = f"data//{file.PatientName}//{file.StudyInstanceUID}//{file.SeriesInstanceUID}"
        if not os.path.isdir(path):
            os.makedirs(path)

        dicom.filewriter.dcmwrite(os.path.join(path,file_names[f]), file)

print("list of patients:")
patient_printer(patient_data)
print('CT scan average duration:', average(scan_duration))
print('Number of hospitals:', len(hospitals))

